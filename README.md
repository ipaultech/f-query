# Fast Query

Ein Tool um schneller zu finden was ich wirklich Suche!

## Preview

![image](/demo.gif)

Teste es auf [Q.Berz.DEV](https://q.berz.dev)

## Beschreibung

**Warum genau dieses Tool?** \
Ich war es leid auf unterschiedlichen Geräten ständig andere Suchmaschinen\
zu verwenden.

Welche meine *Freiheit* und *Privatsphäre* verletzen.

Mit diesem Tool **aggregiere** ich diverse Suchmaschinen,\
damit ich Sie überall schnell benutzen kann.

Dazu verwende ich **!BANGS** wie diesen zum Beispiel diesen:
```
  !yt BeliebigeYoutubeSuchAnfrage
```
Um eine YouTube Suche auf einer [Invidio.us](https://Invidio.us) Instanz zu starten.


Oder auch diesen:
```
  !osm New York
```
Um auf OpenStreeMap nach *"New York"* zu suchen.

Die Suche lässt sich über folgenden Querry in den eigenen Browser einbinden:
```
  [INSTANCE URL]?q=
```
q (requiered) = Search Query