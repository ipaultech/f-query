<?php

set_error_handler(function(int $errno, string $errstr) {
    if ((strpos($errstr, 'Undefined array key') === false) && (strpos($errstr, 'Undefined variable') === false)) {
        return false;
    } else {
        return true;
    }
}, E_WARNING);

#Vars
$file_tags = 'tags.csv';
#CSV Formant is:
#bang(string),url(string),add_quest(boolean0|1)


function tags($tag, $quest){
	#Load CSV into an array
	$csvData = file_get_contents('tags.csv');
	$lines = explode(PHP_EOL, $csvData);
	$tag_array = array();
	foreach ($lines as $line) {
		$tag_array[] = str_getcsv($line);
	}

	#Search requested bang
	foreach($tag_array as $item) {
    	if ($item[0] == $tag) {
			#Bang found
			if ($item[2] == 0) {
				return $item[1];
			}
        	if ($item[2] == 1) {
				return $item[1] .$quest;
				}
	    	}
  	}
}


$get_quest = $_GET['q'];
$get_default_engine = $_GET['deng'];

if ($get_quest ==  null) {
	echo file_get_contents("/var/www/html/site.html");
}
elseif (substr($get_quest, 0, 1) == '!') {
	#Tag Extraction
	$tag = substr($get_quest, 1);
	$tag = strtok($tag, ' ');

	#Question Extraction
	$taglength = strlen($tag)+2;
	$quest = substr($get_quest, $taglength);

	$url = tags($tag, $quest);
	header('Location:' .$url);
}
else {
	if ($get_default_engine == null) {
		$url = 'https://searx.tiekoetter.com/search?language=all&q=' .$get_quest;
		header('Location:' .$url);
	}
	else {
		$url = tags($get_default_engine, $get_quest);
		header('Location:' .$url);
	}
}
exit();

?>
